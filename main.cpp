#include <iostream>
#include <utility>

class Animal
{
protected:
    std::string sound;
    explicit Animal(std::string _sound): sound(std::move(_sound)) {};
public:
    void Voice() {
        std::cout << sound << std::endl;
    }
};

class Dog: public Animal
{
public:
    Dog(): Animal("Woof!") {}
};

class Cat: public Animal
{
public:
    Cat(): Animal("Meow!") {}
};

class Parrot: public Animal
{
public:
    Parrot(): Animal("Pallundrrrrra!") {}
};


int main() {
    Animal* animals [3] = {
            new Dog,
            new Cat,
            new Parrot
    };

    for (auto animal: animals) {
        animal->Voice();
    }

    return 0;
}
